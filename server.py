#!/usr/bin/python3
# Ignacio Cruz de la Haza
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import json
import socketserver
import sys
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    SIP Register Handler class
    """
    dic = {}

    def json2registered(self):
        """
        Comprueba si el fichero registered.json
        ya existe y reutiliza el diccionario
        """
        try:
            with open('registered.json') as file:
                self.dic = json.load(file)
        except FileNotFoundError:
            pass

    def comparacion(self):
        """
        Comprueba si alguno de los
        usuarios tiene el tiempo expirado
        y lo borra
        """
        lista_temporal = []
        for user in self.dic:
            actual_time = time.strftime('%Y-%m-%d %H:%M:%S %z',
                                        time.gmtime(time.time()))
            actual_time2 = time.strptime(actual_time, '%Y-%m-%d %H:%M:%S %z')
            if actual_time2 > time.strptime(self.dic[user]['expires'],
                                            '%Y-%m-%d %H:%M:%S %z'):
                lista_temporal.append(user)
        for user in lista_temporal:
            del self.dic[user]

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        dic2 = {}
        self.json2registered()
        for line in self.rfile:
            message = line.decode('utf-8')
            if message == "\r\n":
                continue
            if message == "\n":
                continue
            print(message)
            palabras = message.split()
            if palabras[0] not in ["REGISTER", "Expires:"]:
                print("ERROR 404, NOT REGISTER")
            if palabras[0] == "Expires:":
                if int(palabras[1]) == 0:
                    del self.dic[user]
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                else:
                    tiempo = time.strftime('%Y-%m-%d %H:%M:%S %z',
                                            time.gmtime(time.time() + int(palabras[1])))
                    dic2['expires'] = tiempo
                    self.comparacion()
                    self.register2json()
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                continue
            if palabras[2] != "SIP/2.0":
                print("ERROR 404, NOT SIP/2.0", palabras[2])
            palabras2 = palabras[1].split(':')
            user = palabras2[-1]
            dic2['address'] = self.client_address[0]
            self.dic[user] = dic2
            print(user)

    def register2json(self):
        """
        Crea el fichero registered.json
        donde guarda todo nuestro diccionario
        """
        with open("registered.json", "w") as f:
            json.dump(self.dic, f, indent=1)
        print(self.dic)


if __name__ == "__main__":
    """
    Programa principal
    """
    if len(sys.argv) != 2:
        sys.exit('Usage: python3 server.py puerto')
    port = int(sys.argv[1])

    # Listens at localhost ('') port 6001
    # and calls the SIPRegisterHandler class to manage the request
    serv = socketserver.UDPServer(('', port), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
